FROM node:alpine

LABEL Author="Madhur Jajoo"
LABEL version="0.0.0"

WORKDIR /srv

EXPOSE 3000

COPY package.json .

RUN npm set progress=false && \
  npm install --no-audit --quiet && \
  npm cache clean --force

COPY . /srv

CMD [ "npm", "run", "docker" ]
