'use strict';

const debug = require('debug')('config:app');

module.exports = function () {
    const express = require('express');
    const path = require('path');
    const bodyParser = require('body-parser');
    const cookieParser = require('cookie-parser');
    const cors = require('cors');

    require('./mongoose')();
    // const routes = require('./../routes');

    const app = express();

    app.use(
        cors({
            origin: ['http://localhost:4200', 'http://client.hoston.local'],
            credentials: true,
        })
    );
    app.enable('trust proxy');
    app.use(cookieParser());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(express.static(path.join(__dirname, './../public/pages')));

    // app.use(routes);
}