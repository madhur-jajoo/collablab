'use strict';

const debug = require('debug')('index');
const http = require('http');

// registering the models
require('./models');

const express = require('./configs/express');
const config = require('./configs/config');

const server = http.createServer(express());

server.listen(config.PORT);

server.on('listening', () => {
    debug(`Server started on port ${config.PORT} in ${config.NODE_ENV} mode`);
    console.log(
        `Server started on port ${config.PORT} in ${config.NODE_ENV} mode`
    );
});
server.on('error', () => {
    debug(
        `Error starting server on port ${config.PORT} in ${config.NODE_ENV} mode`
    );
    console.error(
        `Error starting server on port ${config.PORT} in ${config.NODE_ENV} mode`
    );
});
server.on('close', () => {
    debug(`Server on port ${config.PORT} in ${config.NODE_ENV} mode stopped !`);
    console.log(
        `Server on port ${config.PORT} in ${config.NODE_ENV} mode stopped !`
    );
});